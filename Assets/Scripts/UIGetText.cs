﻿using UnityEngine;
using TMPro;

/// <summary>
/// UI view for getting a text from JS plugin
/// </summary>
public class UIGetText : MonoBehaviour
{
    // Reference to the label
    [SerializeField]
    private TextMeshProUGUI text;

    /// <summary>
    /// Get a text from the JS plugin
    /// </summary>
    public void GetText()
    {
        // Gets and sets a text in the label
        text.text = WebGLPluginJS.GetTextValue();
    }
}
