﻿using UnityEngine;

/// <summary>
/// UI view for calling a function from JS plugin
/// </summary>
public class UICallFunction : MonoBehaviour
{
    /// <summary>
    /// Call function in the JS plugin
    /// </summary>
    public void CallFunction()
    {
        // Calling JS function
        WebGLPluginJS.CallFunction();
    }
}
