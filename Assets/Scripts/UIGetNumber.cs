﻿using UnityEngine;
using TMPro;

/// <summary>
/// UI view for getting a number from JS plugin
/// </summary>
public class UIGetNumber : MonoBehaviour
{
    // Reference to the label
    [SerializeField]
    private TextMeshProUGUI text;

    /// <summary>
    /// Get a number from the JS plugin
    /// </summary>
    public void GetNumber()
    {
        // Gets and sets a number in the label
        text.text = WebGLPluginJS.GetNumberValue().ToString();
    }
}
