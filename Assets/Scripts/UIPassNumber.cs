﻿using UnityEngine;
using TMPro;

/// <summary>
/// UI view for passing number to JS plugin
/// </summary>
public class UIPassNumber : MonoBehaviour
{
    // Reference to the input field
    [SerializeField]
    private TMP_InputField input;

    /// <summary>
    /// Passes the number to the JS plugin
    /// </summary>
    public void PassNumber()
    {
        // If empty, skip it
        if (input.text.Length == 0) return;

        // Get number, parse it, and pass to the plugin
        var num = input.text;
        WebGLPluginJS.PassNumberParam(int.Parse(num));
    }
}
